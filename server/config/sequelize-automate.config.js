const dotenv = require('dotenv');

// 根据 NODE_ENV 加载不同的环境变量文件
const envFile = process.env.NODE_ENV === 'production' ? './.env.production' : './.env.development';
dotenv.config({ path: envFile });

// 自动同步数据库模型配置
module.exports = {
    dbOptions: {
      database: process.env.DB_DATABASE,
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      dialect: process.env.DB_TYPE,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      logging: false
    },
    options: {
      type: "js",
      dir: "models/v1/mapping"
   }
}