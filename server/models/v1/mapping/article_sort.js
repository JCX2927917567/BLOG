const {
  DataTypes
} = require('sequelize');
module.exports = sequelize => {
  const attributes = {
    article_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: false,
      comment: "文章ID",
      field: "article_id"
    },
    sort_id: {
      type: DataTypes.INTEGER(20),
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: false,
      comment: "分类ID",
      field: "sort_id"
    }
  };
  const options = {
    tableName: "article_sort",
    comment: "",
    indexes: []
  };
  const ArticleSortModel = sequelize.define("article_sort_model", attributes, options);
  return ArticleSortModel;
};