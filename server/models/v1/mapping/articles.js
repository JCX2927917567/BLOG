const {
  DataTypes
} = require('sequelize');
module.exports = sequelize => {
  const attributes = {
    articleId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: false,
      comment: "博文ID",
      field: "articleId"
    },
    userId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "发表用户ID",
      field: "userId"
    },
    article_title: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "博文标题",
      field: "article_title"
    },
    article_content: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "博文内容",
      field: "article_content"
    },
    article_views: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "浏览量",
      field: "article_views"
    },
    article_comment_count: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "评论总数",
      field: "article_comment_count"
    },
    article_date: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "发表时间",
      field: "article_date"
    },
    article_like_count: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "点赞数",
      field: "article_like_count"
    }
  };
  const options = {
    tableName: "articles",
    comment: "",
    indexes: []
  };
  const ArticlesModel = sequelize.define("articles_model", attributes, options);
  return ArticlesModel;
};