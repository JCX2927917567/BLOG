const {
  DataTypes
} = require('sequelize');
module.exports = sequelize => {
  const attributes = {
    article_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: false,
      comment: "文章ID",
      field: "article_id"
    },
    label_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "标签ID",
      field: "label_id"
    }
  };
  const options = {
    tableName: "article_label",
    comment: "",
    indexes: []
  };
  const ArticleLabelModel = sequelize.define("article_label_model", attributes, options);
  return ArticleLabelModel;
};