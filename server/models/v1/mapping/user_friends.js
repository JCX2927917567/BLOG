const {
  DataTypes
} = require('sequelize');
module.exports = sequelize => {
  const attributes = {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: false,
      comment: "标识ID",
      field: "id"
    },
    user_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "用户ID",
      field: "user_id"
    },
    user_friends_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "好友ID",
      field: "user_friends_id"
    },
    user_note: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "好友备注",
      field: "user_note"
    },
    user_sattus: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: "好友状态",
      field: "user_sattus"
    }
  };
  const options = {
    tableName: "user_friends",
    comment: "",
    indexes: []
  };
  const UserFriendsModel = sequelize.define("user_friends_model", attributes, options);
  return UserFriendsModel;
};