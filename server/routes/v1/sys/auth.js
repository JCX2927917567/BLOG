/**
 *@author Crystal
 *@date 2023/12/29
 *@Description:后台相关的接口
 */

const express = require('express');
const router = express.Router();
const AuthController = require('@controllers/v1/sys/AuthController');

/**
 * 验证码
 * @route GET /v1/sys/auth/captcha
 * @group 权限验证 - 登录注册相关
 * @returns {object} 200
 * @returns {Error}  default - Unexpected error
 */
router.get('/captcha', AuthController.captcha);


/**
 * @typedef registerUser
 * @property  {string} nickname.required 昵称
 * @property  {string} username.required 用户名
 * @property  {string} password.required 密码
 * @property  {string} email 邮箱
 */

 /** 
 * 注册
 * @route POST /v1/sys/auth/register
 * @group 权限验证 - 登录注册相关
 * @param {registerUser.model} user.body.required - 用户
 * @consumes application/json application/xml
 * @produces application/json application/xml
 * @returns {object} 200 - {"status": 1,"message": "注册成功.","data": {...},"time": 1680598858753}
 * @returns {Error}  default - Unexpected error
 */
router.post('/register', AuthController.register);


/**
 * @typedef loginUser
 * @property  {string} username.required 用户名
 * @property  {string} password.required 密码
 * @property  {string} code 验证码
 */

 /** 
 * 登录
 * @route POST /v1/sys/auth/login
 * @group 权限验证 - 登录注册相关
 * @param {loginUser.model} user.body.required - 用户
 * @consumes application/json application/xml
 * @produces application/json application/xml
 * @returns {object} 200 - {"status": 1,"message": "登录成功.","data": {...},"time": 1680598858753}
 * @returns {Error}  default - Unexpected error
 */
 router.post('/login', AuthController.login);

module.exports = router;
