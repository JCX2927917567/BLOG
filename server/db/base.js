const { Sequelize } = require('sequelize');
const config = require('@/config/db.config');
const chalk = require('chalk');

const sequelize = new Sequelize(config.url, {
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

// 测试数据库连接
sequelize.authenticate()
  .then(() => {
    let isDev = process.env.NODE_ENV === 'development';
    console.log(chalk.rgb(123, 45, 67).bold(`连接${isDev ? chalk.blue.bold('开发环境') : chalk.blue.bold('生产环境')}数据库成功：` + chalk.hex('#DEADED').underline(sequelize.config.database)));
  })
  .catch((error) => {
    console.error('Error in Sequelize connection: ' + error);
  });

module.exports = sequelize;